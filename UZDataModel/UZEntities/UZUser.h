//
//  UZUser.h
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"

@interface UZUser : UZValueObject

@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* gender;

@property (nonatomic, retain) NSNumber* skillLevel;
@property (nonatomic, retain) NSNumber* checkinCooldown;
@property (nonatomic, retain) NSNumber* maxBoutLength;
@property (nonatomic, retain) NSNumber* killCount;
@property (nonatomic, retain) NSNumber* winCount;
@property (nonatomic, retain) NSNumber* lossCount;
@property (nonatomic, retain) NSNumber* fleeCount;
@property (nonatomic, retain) NSNumber* totalPoints;
@property (nonatomic, retain) NSNumber* curentPoints;

@end
