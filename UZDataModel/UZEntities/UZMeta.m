//
//  UZMeta.m
//  RestKitTest
//
//  Created by Justin Leger on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZMeta.h"

@implementation UZMeta

@synthesize code;
@synthesize type;
@synthesize message;

#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping* _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZMeta Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZMeta class]] retain];
        
        //_mapping.rootKeyPath = @"meta";
        
        [_mapping mapKeyPathsToAttributes:
         
             @"code", @"code",
             @"type", @"type",
             @"message", @"message",
         
         nil];
        
        //[[UZServiceManager sharedInstance].manager.mappingProvider registerObjectMapping:_mapping withRootKeyPath:@"meta"];
        
        //[[RKObjectManager sharedManager].mappingProvider registerObjectMapping:_mapping withRootKeyPath:@"meta"];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZMeta mapping];
}



@end
