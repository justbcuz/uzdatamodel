//
//  UZUserSelf.h
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUser.h"

@interface UZUserSelf : UZUser

@property (nonatomic, retain) NSString* password;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* authToken;

@property (nonatomic, retain) NSNumber* loginCount;
@property (nonatomic, retain) NSNumber* previousLoginDate;

@end
