//
//  FSCategory.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FSCategory.h"

@implementation FSCategory

@synthesize fsID;
@synthesize name;
@synthesize pluralName;
@synthesize shortName;
@synthesize iconURL;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping* _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The FSCategory Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[FSCategory class]] retain];
        
        [_mapping mapKeyPathsToAttributes:
             
             @"key", @"identifier",
             @"inserted", @"createdDate",
             @"updated", @"updatedDate",
             
             @"fsId", @"fsID",
             
             @"name", @"name",
             @"plural", @"pluralName",
             @"short", @"shortName",
             @"icon", @"iconURL",
             
         nil];
        
//        [[RKObjectManager sharedManager].mappingProvider registerObjectMapping:_mapping withRootKeyPath:@"response.fsCategories"];
//        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forKeyPath:@"response.fsCategory"];
//        
//        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forResourcePathPattern:@"/v1/fsCategories/:identifier"];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [FSCategory mapping];
}

@end
