//
//  UZCategory.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZCategory.h"

@implementation UZCategory

@synthesize name;
@synthesize description;
@synthesize iconURL;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping* _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZCategory Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZCategory class]] retain];
        
        [_mapping mapKeyPathsToAttributes:
         
             @"key", @"identifier",
             @"inserted", @"createdDate",
             @"updated", @"updatedDate",
             
             @"name", @"name",
             @"desc", @"description",
             @"icon", @"iconURL",
         
         nil];
        
        [[RKObjectManager sharedManager].mappingProvider registerObjectMapping:_mapping withRootKeyPath:@"response.fsCategories"];
        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forKeyPath:@"response.fsCategory"];
        
        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forResourcePathPattern:@"/v1/categories/:identifier"];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZCategory mapping];
}

@end
