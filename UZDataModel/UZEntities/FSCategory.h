//
//  FSCategory.h
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"

@interface FSCategory : UZValueObject

@property (nonatomic, retain) NSString* fsID;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* pluralName;
@property (nonatomic, retain) NSString* shortName;
@property (nonatomic, retain) NSString* iconURL;

@end