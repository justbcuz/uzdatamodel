//
//  UZObject.h
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

#import "UZServiceManager.h"

@interface UZObject : NSObject

#pragma mark -
#pragma mark RestKit Mapping Builder

+ (RKObjectMapping *) mapping;

@end
