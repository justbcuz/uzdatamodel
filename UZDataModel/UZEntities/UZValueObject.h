//
//  UZValueObject.h
//  RestKitTest
//
//  Created by Justin Leger on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>

#import "UZServiceManager.h"

//@interface UZValueObject : NSManagedObject
@interface UZValueObject : NSObject

@property (nonatomic, retain) NSString* identifier;
@property (nonatomic, retain) NSDate* createdDate;
@property (nonatomic, retain) NSDate* updatedDate;

+ (RKObjectMapping *) mapping;

@end
