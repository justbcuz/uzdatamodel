//
//  UZMeta.h
//  RestKitTest
//
//  Created by Justin Leger on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UZObject.h"
#import "UZServiceManager.h"

@interface UZMeta : UZObject

@property (nonatomic, retain) NSString* code;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSString* message;

@end
