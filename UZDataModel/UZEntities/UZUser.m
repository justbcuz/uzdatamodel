//
//  UZUser.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUser.h"

@implementation UZUser

@synthesize username;
@synthesize gender;

@synthesize skillLevel;
@synthesize checkinCooldown;
@synthesize maxBoutLength;
@synthesize killCount;
@synthesize winCount;
@synthesize lossCount;
@synthesize fleeCount;
@synthesize totalPoints;
@synthesize curentPoints;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping* _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZUser Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZUser class]] retain];
        
        [_mapping mapKeyPathsToAttributes:
         
             @"key", @"identifier",
             @"inserted", @"createdDate",
             @"updated", @"updatedDate",
             
             @"un", @"username",
             @"gender", @"gender",
             
             @"skill", @"skillLevel",
             @"cooldown", @"checkinCooldown",
             @"boutLength", @"maxBoutLength",
             @"kill", @"killCount",
             @"win", @"winCount",
             @"loss", @"lossCount",
             @"flee", @"fleeCount",
             
             @"total", @"totalPoints",
             @"current", @"curentPoints",
         
         nil];
        
        [[RKObjectManager sharedManager].mappingProvider setMapping:_mapping forKeyPath:@"response.user"];
        [[RKObjectManager sharedManager].mappingProvider setMapping:_mapping forKeyPath:@"response.users"];
        [[RKObjectManager sharedManager].mappingProvider setMapping:_mapping forKeyPath:@"response.friends"];
        
        //[[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forResourcePathPattern:@"/v1/places/:identifier"];
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{   
    [super initialize];
    
    [UZUser mapping];
}

@end
