//
//  UZUserSelf.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUserSelf.h"

@implementation UZUserSelf

@synthesize password;
@synthesize email;
@synthesize authToken;

@synthesize loginCount;
@synthesize previousLoginDate;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping* _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZUserSelf Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZUserSelf class]] retain];
        
        [_mapping mapKeyPathsToAttributes:
         
             @"key", @"identifier",
             @"inserted", @"createdDate",
             @"updated", @"updatedDate",
             
             @"un", @"username",
             @"gender", @"gender",
             
             @"pw", @"password",
             @"email", @"email",
             @"authToken", @"authToken",
             
             @"skill", @"skillLevel",
             @"cooldown", @"checkinCooldown",
             @"boutLength", @"maxBoutLength",
             @"kill", @"killCount",
             @"win", @"winCount",
             @"loss", @"lossCount",
             @"flee", @"fleeCount",
             
             @"total", @"totalPoints",
             @"current", @"curentPoints",
         
         nil];
        
//        [[RKObjectManager sharedManager].mappingProvider registerObjectMapping:_mapping withRootKeyPath:@"response.fsCategories"];
//        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forKeyPath:@"response.fsCategory"];
//        
//        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forResourcePathPattern:@"/v1/fsCategories/:identifier"];
        
//        [[RKObjectManager sharedManager].mappingProvider setMapping:_mapping forKeyPath:@"response.user"];
//        [[RKObjectManager sharedManager].mappingProvider setMapping:_mapping forKeyPath:@"response.users"];
//        [[RKObjectManager sharedManager].mappingProvider setMapping:_mapping forKeyPath:@"response.friends"];
        
//        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forResourcePathPattern:@"/v1/places/:identifier"];
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZUserSelf mapping];
}

@end
