//
//  UZPlace.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZPlace.h"

@implementation UZPlace

@synthesize fsKey;
@synthesize categoryKey;
@synthesize fsCategoryKey;

@synthesize name;
@synthesize phone;
@synthesize address;
@synthesize city;
@synthesize state;
@synthesize zipcode;
@synthesize country;

@synthesize latitude;
@synthesize longitude;
@synthesize distance;

@synthesize boutCount;
@synthesize targetCount;

@synthesize category;
@synthesize fsCategory;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping* _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZPlace Mapping");
    
        _mapping = [[RKObjectMapping mappingForClass:[UZPlace class]] retain];
        
        //_mapping.rootKeyPath = @"response.places";
        
        [_mapping mapKeyPathsToAttributes:
         
             @"key", @"identifier",
             @"inserted", @"createdDate",
             @"updated", @"updatedDate",
             
             @"fsKey", @"fsKey",
             
             @"categoryKey", @"categoryKey",
             @"fsCategoryKey", @"fsCategoryKey",
             
             @"name", @"name",
             @"phone", @"phone",
             @"address", @"address",
             @"city", @"city",
             @"stateCode", @"state",
             @"zipcode", @"zipcode",
             @"countryCode", @"country",
             
             @"lat", @"latitude",
             @"lng", @"longitude",
             
             @"distance", @"distance",
             @"boutCount", @"boutCount",
             @"targetCount", @"targetCount",
         
         nil];
        
        [_mapping mapRelationship:@"category" withMapping:[UZCategory mapping]];
        [_mapping mapRelationship:@"fsCategory" withMapping:[FSCategory mapping]];
        
//        [[RKObjectManager sharedManager].mappingProvider registerObjectMapping:_mapping withRootKeyPath:@"response.places"];
//        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forKeyPath:@"response.place"];
        
        // Technically don't need this line because the registerObjectMapping does it as well.
        //[[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forKeyPath:@"response.places"];
        
        
//        [[RKObjectManager sharedManager].mappingProvider setObjectMapping:_mapping forResourcePathPattern:@"/v1/places/:identifier"];
        
    } else {
        NSLog(@"I am NOT building The UZPlace Mapping it exists already");
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZPlace mapping];
}



@end
