//
//  UZPlace.h
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"
#import "FSCategory.h"
#import "UZCategory.h"

@interface UZPlace : UZValueObject

@property (nonatomic, retain) NSString* fsKey;
@property (nonatomic, retain) NSString* categoryKey;
@property (nonatomic, retain) NSString* fsCategoryKey;

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* phone;
@property (nonatomic, retain) NSString* address;
@property (nonatomic, retain) NSString* city;
@property (nonatomic, retain) NSString* state;
@property (nonatomic, retain) NSString* zipcode;
@property (nonatomic, retain) NSString* country;

@property (nonatomic, retain) NSNumber* latitude;
@property (nonatomic, retain) NSNumber* longitude;
@property (nonatomic, retain) NSNumber* distance;

@property (nonatomic, retain) NSNumber* boutCount;
@property (nonatomic, retain) NSNumber* targetCount;

@property (nonatomic, retain) UZCategory* category;
@property (nonatomic, retain) FSCategory* fsCategory;

@end