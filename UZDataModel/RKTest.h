//
//  RKTest.h
//  UZDataModel
//
//  Created by Justin Leger on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <RestKit/RestKit.h>

#import "UZUser+Services.h"
#import "UZUserSelf+Services.h"

@interface RKTest : NSObject

+(void) runRKTests;
+(void) getFriends:(UZUserSelf *) aUser;

@end
