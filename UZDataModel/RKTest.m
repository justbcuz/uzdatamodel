//
//  RKTest.m
//  UZDataModel
//
//  Created by Justin Leger on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RKTest.h"

@implementation RKTest

//- (id)init {
//    self = [super init];
//	if (self) {
//        
//    }
//    
//	return self;
//}

+(void) runRKTests
{
    [UZUserSelf loginWithUsername:@"user107" andPassword:@"password"
     
                   onSuccessBlock:^(UZResponse * response) {
                       UZUserSelf * me = (UZUserSelf *) response.response;
                       [RKTest getFriends:me];
                       NSLog(@"Auth Token: %@", me.authToken);
                   }
     
                   onFailureBlock:^(NSError * error) {
                       NSLog(@"Log In Failed. Error: %@", error.description);
                   }
     ];
    
    [UZUserSelf loginWithAuthToken:@"8EwssOSdhPNuPbORfXU18YlmE6fhw7SxfmmY0f0JVQBHVp27jU5iuPYZNADHC1fLn5" 
                   onSuccessBlock:^(UZResponse * response) {
                       UZUserSelf * me = (UZUserSelf *) response.response;
                       NSLog(@"Auth Token: %@", me.authToken);
                   }
                   onFailureBlock:^(NSError * error) {
                       NSLog(@"Error: %@", error.description);
                   }
     ];
}

+(void) getFriends:(UZUserSelf *) aUser {
    
    [aUser friendsWithAuthToken:aUser.authToken
     
                 onSuccessBlock:^(UZResponse * response) {
                     NSArray * friends = (NSArray *) response.response;
                     NSLog(@"Friends Count: %i", [friends count]);
                 }
     
                 onFailureBlock:^(NSError * error) {
                     NSLog(@"Log In Failed. Error: %@", error.description);
                 }
     ];
}

@end
