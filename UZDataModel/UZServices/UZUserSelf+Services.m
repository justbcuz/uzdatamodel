//
//  UZUser+Services.m
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUserSelf+Services.h"
#import "UZUser+Services.h"

#pragma mark - Private Methods Declaration
@interface UZUserSelf ()

+ (void) loginWithDictionary:(NSMutableDictionary*) aDictionary onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock;

// Callback Methods
+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock;
+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects onSuccessBlock:(void(^)(NSArray *))aSuccessBlock;
+ (void) objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error onFailureBlock:(void(^)(NSError *))aFailureBlock;

@end

@implementation UZUserSelf (Services)

#pragma mark - Service Methods
#pragma mark - Login Service Methods

+ (void) loginWithUsername:(NSString*) aUsername andPassword:(NSString *) aPassword onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock
{
    // Define the extra query string parameters
    NSMutableDictionary * requestParams = [[NSMutableDictionary alloc] init]; 
    
    // Append the query string parameters to the URL
    [requestParams setValue:aUsername forKey:@"un"];
    [requestParams setValue:aPassword forKey:@"pw"];
    
    [UZUserSelf loginWithDictionary:requestParams onSuccessBlock:aSuccessBlock onFailureBlock:aFailureBlock];
    
    [requestParams release];
}

+ (void) loginWithAuthToken:(NSString*) aAuthToken onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock
{
    // Define the extra query string parameters
    NSMutableDictionary * requestParams = [[NSMutableDictionary alloc] init]; 
    
    // Append the query string parameters to the URL
    [requestParams setValue:aAuthToken forKey:@"at"];
    
    [UZUserSelf loginWithDictionary:requestParams onSuccessBlock:aSuccessBlock onFailureBlock:aFailureBlock];
    
    [requestParams release];
}

#pragma mark - Private Methods

+ (void) loginWithDictionary:(NSDictionary*) aDictionary onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock
{
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    NSString * resourcePath = [@"/v1/users/login" stringByAppendingQueryParameters:aDictionary];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        [loader.mappingProvider registerObjectMapping:[UZResponse mappingWithKeyPath:@"response.user" andMapping:[UZUserSelf mapping]] withRootKeyPath:@""];
        
        // Define the Callback blocks
        
        loader.onDidLoadObject  = ^(id object) {
            [UZUserSelf objectLoader:loader didLoadObject:object onSuccessBlock:aSuccessBlock];
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZUserSelf objectLoader:loader didFailWithError:error onFailureBlock:aFailureBlock];
        };
    }];
}


#pragma mark - Private Callback Methods

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock
{
    aSuccessBlock(object);
}

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects onSuccessBlock:(void(^)(NSArray *))aSuccessBlock
{
    aSuccessBlock(objects);
}

+ (void) objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error onFailureBlock:(void(^)(NSError *))aFailureBlock
{
    aFailureBlock(error);
}

@end
