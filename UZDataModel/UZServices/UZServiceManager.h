//
//  UZServiceManager.h
//  RestKitTest
//
//  Created by Justin Leger on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

#import "SynthesizeSingleton.h"

@interface UZServiceManager : NSObject

SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(UZServiceManager);

@property (nonatomic, retain) RKObjectManager * manager;

@end
