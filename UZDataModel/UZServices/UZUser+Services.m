//
//  UZUser+Services.m
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUser+Services.h"

#pragma mark - Private Methods Declaration
@interface UZUser ()

+ (void) loginWithDictionary:(NSMutableDictionary*) aDictionary onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock;

// Callback Methods
+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock;
+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects onSuccessBlock:(void(^)(NSArray *))aSuccessBlock;
+ (void) objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error onFailureBlock:(void(^)(NSError *))aFailureBlock;

@end

@implementation UZUser (Services)

#pragma mark - Friend Service Methods

- (void) friendsWithAuthToken:(NSString*) aAuthToken onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock
{
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    // Define the extra query string parameters
    NSMutableDictionary * requestParams = [[NSMutableDictionary alloc] init]; 
    
    // Append the query string parameters to the URL
    [requestParams setValue:aAuthToken forKey:@"at"];
    
    NSString * identString = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
    
    NSString * resourcePath = [[NSString stringWithFormat:@"/v1/users/%@/friends", identString] stringByAppendingQueryParameters:requestParams];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        [loader.mappingProvider registerObjectMapping:[UZResponse mappingWithKeyPath:@"response.friends" andMapping:[UZUser mapping]] withRootKeyPath:@""];
        
        // Define the Callback blocks
        
        loader.onDidLoadObject  = ^(id object) {
            [UZUser objectLoader:loader didLoadObject:object onSuccessBlock:aSuccessBlock];
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZUser objectLoader:loader didFailWithError:error onFailureBlock:aFailureBlock];
        };
    }];
    
    [requestParams release];
}

#pragma mark - Private Callback Methods

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock
{
    aSuccessBlock(object);
}

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects onSuccessBlock:(void(^)(NSArray *))aSuccessBlock
{
    aSuccessBlock(objects);
}

+ (void) objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error onFailureBlock:(void(^)(NSError *))aFailureBlock
{
    aFailureBlock(error);
}

@end
