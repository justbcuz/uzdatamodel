//
//  UZServiceManager.m
//  RestKitTest
//
//  Created by Justin Leger on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZServiceManager.h"

#pragma mark - Private methods declaration
@interface UZServiceManager ()
- (void)initializeRestKit;
@end


@implementation UZServiceManager

SYNTHESIZE_SINGLETON_FOR_CLASS(UZServiceManager);

@synthesize manager;

- (id)init {
    self = [super init];
	if (self) {
        
        [self initializeRestKit];
        
    }
    
	return self;
}

#pragma mark - Private methods

- (void)initializeRestKit {
    
    // Configure RestKit logging
    RKLogConfigureByName("RestKit", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/Network", RKLogLevelDebug);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelDebug);
    RKLogConfigureByName("RestKit/Network/Queue", RKLogLevelDebug);
    // RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    
    // Define the shared objectManager
    self.manager = [RKObjectManager objectManagerWithBaseURLString:@"http://api.urbanzombie.dev"];
    [self.manager.requestQueue setShowsNetworkActivityIndicatorWhenBusy:YES];
    
    // Setup the MIME types
    self.manager.acceptMIMEType = RKMIMETypeJSON;
    self.manager.serializationMIMEType = RKMIMETypeJSON;
}

@end
