//
//  UZUser+Services.h
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UZServiceManager.h"
#import "UZResponse.h"

#import "UZUser.h"
#import "UZUserSelf.h"

@interface UZUser (Services)

//+ (UZUser*) userWithKey;
//+ (NSArray*) friends;
//
//- (void) save;
//- (void) reload;

- (void) friendsWithAuthToken:(NSString*) aAuthToken onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock;

@end
