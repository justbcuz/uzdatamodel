//
//  UZUser+Services.h
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UZServiceManager.h"
#import "UZResponse.h"

#import "UZUserSelf.h"

@interface UZUserSelf (Services)

+ (void) loginWithUsername:(NSString*) aUsername andPassword:(NSString *) aPassword onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock;
+ (void) loginWithAuthToken:(NSString*) aAuthToken onSuccessBlock:(void(^)(UZResponse *))aSuccessBlock onFailureBlock:(void(^)(NSError *))aFailureBlock;

@end
