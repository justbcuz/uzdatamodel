//
//  UZResponse.m
//  RestKitTest
//
//  Created by Justin Leger on 3/18/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import "UZResponse.h"

@implementation UZResponse

@synthesize meta;
@synthesize response;

#pragma mark -
#pragma mark RestKit Mapping Builder

+ (RKObjectMapping *) mapping
{
    NSLog(@"Making UZResponse Mapping");
    
    RKObjectMapping * _mapping = [RKObjectMapping mappingForClass:[UZResponse class]];

    [_mapping mapRelationship:@"meta" withMapping:[UZMeta mapping]];
    
    return _mapping;
}

+ (RKObjectMapping *) mappingWithKeyPath:(NSString *)aKeyPath andMapping:(RKObjectMapping *)aMapping;
{
    RKObjectMapping * _mapping =[UZResponse mapping];
    
    NSLog(@"Adding UZResponse Relationship");
    
    [_mapping mapKeyPath:aKeyPath toRelationship:@"response" withMapping:aMapping];
    
    return _mapping;
}


@end
