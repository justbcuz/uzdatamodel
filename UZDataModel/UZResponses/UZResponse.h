//
//  UZResponse.h
//  RestKitTest
//
//  Created by Justin Leger on 3/18/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import "UZObject.h"
#import "UZMeta.h"

@interface UZResponse : UZObject

@property (nonatomic, retain) UZMeta * meta;
@property (nonatomic, retain) id response;

+ (RKObjectMapping *) mappingWithKeyPath:(NSString *)aKeyPath andMapping:(RKObjectMapping *)aMapping;

@end
