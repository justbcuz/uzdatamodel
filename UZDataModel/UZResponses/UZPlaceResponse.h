//
//  UZResponse.h
//  RestKitTest
//
//  Created by Justin Leger on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZObject.h"
#import "UZMeta.h"
#import "UZPlace.h"

@interface UZPlaceResponse : UZObject

@property (nonatomic, retain) UZMeta * meta;
//@property (nonatomic, retain) id response;
@property (nonatomic, retain) id places;

//+ (RKObjectMapping *) responseMapping: (RKObjectMapping *) aMapping;


@end
