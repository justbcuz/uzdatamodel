//
//  UZResponse.m
//  RestKitTest
//
//  Created by Justin Leger on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZPlaceResponse.h"

@implementation UZPlaceResponse

@synthesize meta;
//@synthesize response;
@synthesize places;

#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping* _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        NSLog(@"I am building The UZMeta Mapping");
        
        _mapping = [RKObjectMapping mappingForClass:[UZPlaceResponse class]];
        
        [_mapping mapRelationship:@"meta" withMapping:[UZMeta mapping]];
        [_mapping mapKeyPath:@"response.places" toRelationship:@"places" withMapping:[UZPlace mapping]];
        
        //[[RKObjectManager sharedManager].mappingProvider registerObjectMapping:_mapping withRootKeyPath:@""];
        
        //[_mapping mapKeyPath:@"response.place" toRelationship:@"places" withMapping:[UZPlace mapping]];
        //[_mapping mapRelationship:@"response" withMapping:[UZMeta mapping]];
        
    } else {
        NSLog(@"I am NOT building The UZMeta Mapping it exists already");
    }
    
    return _mapping;
}

//+ (RKObjectMapping *) responseMapping: (RKObjectMapping *) aMapping
//{
//    if (!_mapping) [UZResponse mapping];
//    
//    [_mapping mapRelationship:@"response" withMapping:aMapping];
//    
//    return _mapping;
//}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [UZPlaceResponse mapping];
    NSLog(@"I am The UZMeta Initializer");
}


@end
